const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

const scale = 2;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;
const techWidth = 32;
const techHeight = 32;
const username = localStorage.getItem('username');
const score = localStorage.getItem('score');

let widthTime = 100;
let heightTime = 20;
let max = 100;
let val = 100;
let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 2;
let enemySpeed = 20;
let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomTechX = Math.abs(Math.floor(Math.random() * 7));
let randomTechXSelect = randomTechX * 64;
let randomTechY = Math.abs(Math.floor(Math.random() * 4));
let randomTechYSelect = randomTechY * 64;
let scoreCount = 0;
let character = new Image();
character.src = "assets/img/futureMan.png";

let techSprite = new Image();
techSprite.src = "assets/img/spritesheet2.png";

let enemySprite = new Image();
enemySprite.src = "assets/img/enemy.png";

let enemy = new GameObject(enemySprite, 100, 100, 50, 50); // Initial position and size of the enemy
// Default Player
let player = new GameObject(character, 0, 0, 200, 200);
let tech = new GameObject(techSprite, randomX, randomY, 100, 100);
// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input
let enemies = [];

if (score){
    scoreCount = score;
}

function addName() {
    let header = document.getElementById("main-header");
    header.innerHTML = "Hello " + username;
}

addName();

function randoPos(rangeX, rangeY, delta){
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}

function newTech(){
    randomTechX = Math.abs(Math.floor(Math.random() * 7));
    randomTechXSelect = randomTechX * 64;
    randomTechY = Math.abs(Math.floor(Math.random() * 4));
    randomTechYSelect = randomTechY * 64;
    techPosition = new randoPos(1099, 499, 50);
}

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
    this.type = 'enemy';
}

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    //console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 65: // Left Arrow
            gamerInput = new GamerInput("Left");
            break; //Left key
        case 87: // Up Arrow
            gamerInput = new GamerInput("Up");
            break; //Up key
        case 68: // Right Arrow
            gamerInput = new GamerInput("Right");
            break; //Right key
        case 83: // Down Arrow
            gamerInput = new GamerInput("Down");
            break; //Down key
        case 83:
            speed = 4;
            break;
        default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
        speed = 2;
    }
}


var dynamic = nipplejs.create({
    color: 'grey',
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
     });
});

function spawnEnemies(numEnemies) {
    for (let i = 0; i < numEnemies; i++) {
        let enemyX = Math.abs(Math.floor(Math.random() * canvas.width));
        let enemyY = Math.abs(Math.floor(Math.random() * canvas.height));
        enemies.push(new GameObject(enemySprite, enemyX, enemyY, 50, 50, 'enemy'));
    }
}

spawnEnemies(10);
// Spritesheet atlas references
// row 0 down
// row 1 up
// row 2 left
// row 3 right


function update() {
    // console.log("Update");
    // Check Input
    if (gamerInput.action === "Up") {
        if (player.y < 0){
            console.log("player at top edge");
        }
        else{
            player.y -= speed; // Move Player Up
        }
        currentDirection = 1;
    } else if (gamerInput.action === "Down") {
        if (player.y + scaledHeight > canvas.height){
            console.log("player at bottom edge");
        }
        else{
            player.y += speed; // Move Player Down
        }
        currentDirection = 0;
    } else if (gamerInput.action === "Left") {
        if (player.x < 0){
            console.log("player at left edge");
        }
        else{
            player.x -= speed; // Move Player Left
        }
        currentDirection = 2;
    } else if (gamerInput.action === "Right") {
        if (player.x + scaledWidth > canvas.width){

        }
        else{
            player.x += speed; // Move Player Right
        }
        currentDirection = 3;
    } else if (gamerInput.action === "None") {
    }

    // Check for collisions with enemies
    for (let i = 0; i < enemies.length; i++) {
        if (
            player.x < enemies[i].x + enemies[i].width &&
            player.x + scaledWidth > enemies[i].x &&
            player.y < enemies[i].y + enemies[i].height &&
            player.y + scaledHeight > enemies[i].y
        ) {
            // Collision detected, reset player position
            player.x = 0;
            player.y = 0;
        }
    }

    for (let i = 0; i < enemies.length; i++) {
        if (Math.random() < 0.01) {
            let randomDirection = Math.floor(Math.random() * 4); // 0: Up, 1: Down, 2: Left, 3: Right
    
            switch (randomDirection) {
                case 0:
                    if (enemies[i].y > 0) {
                        enemies[i].y -= enemySpeed;
                    }
                    break;
                case 1:
                    if (enemies[i].y + enemies[i].height < canvas.height) {
                        enemies[i].y += enemySpeed;
                    }
                    break;
                case 2:
                    if (enemies[i].x > 0) {
                        enemies[i].x -= enemySpeed;
                    }
                    break;
                case 3:
                    if (enemies[i].x + enemies[i].width < canvas.width) {
                        enemies[i].x += enemySpeed;
                    }
                    break;
            }
        }
    }
    
}

function drawFrame(image, frameX, frameY, canvasX, canvasY) {
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

techPosition = new randoPos(1099, 499, 50);

function manageTech(){
    // place the piece of the tech pieces
    context.drawImage(techSprite, randomTechX*64, randomTechY*64, 64, 64, techPosition.x, techPosition.y, techWidth, techHeight);
    // check for collision 
   
    if (techPosition.x < player.x + scaledWidth && //collision from left to right
        techPosition.x + techWidth > player.x && // collision from right to left
        techPosition.y < player.y + scaledHeight && // collision from top to bottom
        techPosition.y + techHeight > player.y // collision from bottom to top
        ){
        console.log("collision!");
        scoreCount ++;
        localStorage.setItem("score", scoreCount);
        newTech();
    }
    
}

function writeScore(){
    let scoreString = "score: " + scoreCount;
    context.font = '22px sans-serif';
    context.fillText(scoreString, 1000, 20)
}

function writeLives(){
    let livesString = "lives: " + lives;
    context.font = '22px sans-serif';
    context.fillText(livesString, 1000, 50)
}
function draw() {
    const backgroundImage = new Image();
    backgroundImage.src = "assets/img/prehistoric.png";
    context.drawImage(backgroundImage, 0, 0, canvas.width, canvas.height);
    for (let i = 0; i < enemies.length; i++) {
        context.drawImage(enemies[i].spritesheet, enemies[i].x, enemies[i].y, enemies[i].width, enemies[i].height);
    }
    //context.clearRect(0,0, canvas.width, canvas.height);
    manageTech();
    animate();
    writeScore();
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
